FROM registry.gitlab.com/dedyms/debian:latest
RUN apt update && apt install -y --no-install-recommends python3-pip git && pip3 install --upgrade pip setuptools wheel distlib && apt clean && rm -rf /var/lib/apt/lists/* && apt clean
USER $CONTAINERUSER
WORKDIR /home/$CONTAINERUSER/
RUN git clone --depth=1 https://github.com/barrycarey/Plex-Data-Collector-For-InfluxDB.git plexdatacollector
RUN pip3 install --user -r /home/$CONTAINERUSER/plexdatacollector/requirements.txt && rm -rf /home/$CONTAINERUSER/.cache/pip
WORKDIR /home/$CONTAINERUSER/plexdatacollector/
CMD ["python3", "-u", "plexcollector.py"]
VOLUME /home/$CONTAINERUSER/plexdatacollector/
